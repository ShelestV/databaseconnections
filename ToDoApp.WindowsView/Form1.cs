using Microsoft.EntityFrameworkCore;
using ToDoApp.PersistenceEFDatabaseFirst.Entities;

namespace ToDoApp.WindowsView;

public partial class Form1 : Form
{
    private readonly IDbContextFactory<ToDoDbContext> _dbContextFactory;

    public Form1(IDbContextFactory<ToDoDbContext> dbContextFactory)
    {
        InitializeComponent();

        _dbContextFactory = dbContextFactory;
    }

    private void Form1_Load(object sender, EventArgs e)
    {
        using (var context = _dbContextFactory.CreateDbContext())
            MessageBox.Show(context.Lists.OrderBy(x => x.Id).FirstOrDefault()?.Title);

        using (var context = _dbContextFactory.CreateDbContext())
            MessageBox.Show(context.Lists.OrderBy(x => x.Id).LastOrDefault()?.Title);
    }
}
