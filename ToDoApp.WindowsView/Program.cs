using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ToDoApp.PersistenceEFDatabaseFirst.Entities;

namespace ToDoApp.WindowsView;

internal static class Program
{
    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
        var config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .Build();

        var services = new ServiceCollection();
        ConfigureServices(services, config);

        using var serviceProvider = services.BuildServiceProvider();
        var form = serviceProvider.GetRequiredService<Form1>();
        Application.Run(form);
    }

    private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("DatabaseFirst");
        services.AddDbContext<ToDoDbContext>(options => options.UseSqlServer(connectionString));
        services.AddDbContextFactory<ToDoDbContext>(options => options.UseSqlServer(connectionString));

        services.AddScoped<Form1>();
    }
}