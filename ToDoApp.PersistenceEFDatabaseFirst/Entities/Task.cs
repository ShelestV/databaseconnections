﻿using System;
using System.Collections.Generic;

namespace ToDoApp.PersistenceEFDatabaseFirst.Entities;

public partial class Task
{
    public int Id { get; set; }

    public string Title { get; set; } = null!;

    public string? Description { get; set; }

    public bool? IsCompleted { get; set; }

    public int? ListId { get; set; }

    public virtual List? List { get; set; }
}
