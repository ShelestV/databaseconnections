﻿using System;
using System.Collections.Generic;

namespace ToDoApp.PersistenceEFDatabaseFirst.Entities;

public partial class List
{
    public int Id { get; set; }

    public string Title { get; set; } = null!;

    public string? Description { get; set; }

    public virtual ICollection<Task> Tasks { get; set; } = new List<Task>();
}
