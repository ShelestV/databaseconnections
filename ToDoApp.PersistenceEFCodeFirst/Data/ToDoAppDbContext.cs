﻿using Microsoft.EntityFrameworkCore;
using ToDoApp.Domain.Models;

namespace ToDoApp.PersistenceEFCodeFirst.Data;

public sealed class ToDoAppDbContext : DbContext
{
    private static string? connectionString = "Data Source=.\\SQLEXPRESS;Initial Catalog=ToDoCodeFirstDb;Integrated Security=SSPI;Encrypt=False;TrustServerCertificate=True;Persist Security Info=True;Connection Timeout=30;";

    public static void SetConnectionString(Func<string> getConnectionString)
    {
        if (string.IsNullOrWhiteSpace(connectionString))
            connectionString = getConnectionString();
    }

    public DbSet<ToDoList> Lists { get; set; } = null!;
    public DbSet<ToDoTask> Tasks { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (string.IsNullOrWhiteSpace(connectionString))
            throw new Exception("Connection string is null");

        optionsBuilder.UseSqlServer(connectionString);

        base.OnConfiguring(optionsBuilder);
    }
}
