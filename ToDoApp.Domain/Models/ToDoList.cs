﻿namespace ToDoApp.Domain.Models;

public sealed class ToDoList : Model
{
	public string Title { get; set; } = null!;
	public string? Description { get; set; }
	public IReadOnlyCollection<ToDoTask>? Tasks { get; set; }
}
