﻿namespace ToDoApp.Domain.Models;

public sealed class ToDoTask : Model
{
	public string Title { get; set; } = null!;
	public string? Description { get; set; }
	public bool IsCompleted { get; set; }
	public int? ListId { get; set; }
	public ToDoList? List { get; set; }
}
