﻿namespace ToDoApp.Domain.Models;

public abstract class Model
{
	public int Id { get; set; }
}
