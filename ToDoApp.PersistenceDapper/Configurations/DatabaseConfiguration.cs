﻿namespace ToDoApp.PersistenceDapper.Configurations;

public sealed class DatabaseConfiguration
{
	public string ConnectionString { get; set; } = null!;
}
