﻿using Dapper;
using Microsoft.Data.SqlClient;
using ToDoApp.Application.Shared;
using ToDoApp.Domain.Models;
using ToDoApp.PersistenceDapper.Configurations;

namespace ToDoApp.PersistenceDapper.Repositories;

public abstract class CrudRepository<TModel> : ICrudRepository<TModel> where TModel : Model
{
	private readonly string _connectionString;

	protected abstract string CreateQuery { get; }
	protected abstract string DeleteQuery { get; }
	protected abstract string GetQuery { get; }
	protected abstract string GetAllQuery { get; }
	protected abstract string UpdateQuery { get; }

	protected SqlConnection OpenedConnection
	{
		get
		{
			var connection = new SqlConnection(_connectionString);
			connection.Open();
			return connection;
		}
	}

	public CrudRepository(DatabaseConfiguration config)
	{
		_connectionString = config.ConnectionString;
	}

	public virtual void Create(TModel model)
	{
		using var connection = OpenedConnection;
		connection.Query(CreateQuery, model);
	}

	public virtual void Delete(int id)
	{
		using var connection = OpenedConnection;
		connection.Query(DeleteQuery, new { Id = id });
    }

	public virtual TModel? Get(int id)
	{
		using var connection = OpenedConnection;
		return connection.QueryFirstOrDefault<TModel>(GetQuery, new { Id = id });
	}

	public virtual IReadOnlyCollection<TModel> GetAll()
	{
		using var connection = OpenedConnection;
		return connection.Query<TModel>(GetAllQuery).ToList();
	}

	public virtual void Update(TModel model)
	{
		using var connection = OpenedConnection;
		connection.Query(UpdateQuery, model);
	}
}
