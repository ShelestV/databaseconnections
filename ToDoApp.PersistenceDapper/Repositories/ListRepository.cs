﻿using ToDoApp.Application.Lists;
using ToDoApp.Domain.Models;
using ToDoApp.PersistenceDapper.Configurations;

namespace ToDoApp.PersistenceDapper.Repositories;

public sealed class ListRepository : CrudRepository<ToDoList>, IListRepository
{
	protected override string CreateQuery => "INSERT INTO Lists (Title, Description) VALUES (@Title, @Description); SELECT CAST(SCOPE_IDENTITY() as int);";

	protected override string DeleteQuery => "DELETE FROM Lists WHERE Id = @Id;";

	protected override string GetQuery => "SELECT * FROM Lists WHERE Id = @Id;";

	protected override string GetAllQuery => "SELECT * FROM Lists;";

	protected override string UpdateQuery => "UPDATE Lists SET Title = @Title, Description = @Description WHERE Id = @Id;";

	public ListRepository(DatabaseConfiguration config) : base(config)
	{
	}
}
