﻿using Dapper;
using ToDoApp.Application.Tasks;
using ToDoApp.Domain.Models;
using ToDoApp.PersistenceDapper.Configurations;

namespace ToDoApp.PersistenceDapper.Repositories;

public sealed class TaskRepository : CrudRepository<ToDoTask>, ITaskRepository
{
	protected override string CreateQuery => "INSERT INTO Tasks (Title, Description, ListId) VALUES (@Title, @Description, @ListId); SELECT CAST(SCOPE_IDENTITY() as int);";

	protected override string DeleteQuery => "DELETE FROM Tasks WHERE Id = @Id;";

	protected override string GetQuery => "SELECT * FROM Tasks WHERE Id = @Id;";

	protected override string GetAllQuery => "SELECT * FROM Tasks;";

	protected override string UpdateQuery => "UPDATE Tasks SET Title = @Title, Description = @Description, ListId = @ListId WHERE Id = @Id;";

	public TaskRepository(DatabaseConfiguration config) : base(config)
	{
	}

	public IReadOnlyCollection<ToDoTask> GetByListId(int listId)
	{
		using var connection = OpenedConnection;
		return connection.Query<ToDoTask>(GetAllQuery, new { ListId = listId }).ToList();
	}
}
