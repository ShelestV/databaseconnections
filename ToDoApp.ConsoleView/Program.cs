﻿using Microsoft.Extensions.Configuration;
using ToDoApp.Application.Lists;
using ToDoApp.Application.Tasks;
using ToDoApp.Domain.Models;
using ToDoApp.PersistenceDapper.Configurations;
using ToDoApp.PersistenceDapper.Repositories;

var configuration = new ConfigurationBuilder()
	.SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
	.AddJsonFile("appsettings.json")
	.Build();

var config = new DatabaseConfiguration { ConnectionString = configuration.GetConnectionString("DefaultConnection")! };

var listRepository = new ListRepository(config);
var taskRepository = new TaskRepository(config);

var listService = new ListService(listRepository);
var taskService = new TaskService(taskRepository);

var list = new ToDoList { Id = 7, Title = "My List", Description = "First List" };

listService.Create(list);
var lists = listService.GetAll();
foreach (var item in lists)
{
	Console.WriteLine($"{item.Title} ({item.Description})");
}


