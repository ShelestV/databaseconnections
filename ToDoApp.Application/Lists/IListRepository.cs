﻿using ToDoApp.Application.Shared;
using ToDoApp.Domain.Models;

namespace ToDoApp.Application.Lists;

public interface IListRepository : ICrudRepository<ToDoList>
{
}
