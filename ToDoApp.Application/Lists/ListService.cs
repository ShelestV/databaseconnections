﻿using ToDoApp.Application.Shared;
using ToDoApp.Domain.Models;

namespace ToDoApp.Application.Lists;

public sealed class ListService : CrudService<ToDoList>, IListService
{
	public ListService(IListRepository repository) : base(repository)
	{
	}
}
