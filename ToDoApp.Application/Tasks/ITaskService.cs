﻿using ToDoApp.Application.Shared;
using ToDoApp.Domain.Models;

namespace ToDoApp.Application.Tasks;

public interface ITaskService : ICrudService<ToDoTask>
{
	IReadOnlyCollection<ToDoTask> GetByListId(int listId);
}
