﻿using ToDoApp.Application.Shared;
using ToDoApp.Domain.Models;

namespace ToDoApp.Application.Tasks;

public sealed class TaskService : CrudService<ToDoTask>, ITaskService
{
	private readonly ITaskRepository _repository;

	public TaskService(ITaskRepository repository) : base(repository)
	{
		_repository = repository;
	}

	public IReadOnlyCollection<ToDoTask> GetByListId(int listId)
	{
		return _repository.GetByListId(listId);
	}
}
