﻿using ToDoApp.Application.Shared;
using ToDoApp.Domain.Models;

namespace ToDoApp.Application.Tasks;

public interface ITaskRepository : ICrudRepository<ToDoTask>
{
	IReadOnlyCollection<ToDoTask> GetByListId(int listId);
}
