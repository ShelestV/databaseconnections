﻿using ToDoApp.Domain.Models;

namespace ToDoApp.Application.Shared;

public abstract class CrudService<TModel> : ICrudService<TModel> where TModel : Model
{
	private readonly ICrudRepository<TModel> _repository;

	protected CrudService(ICrudRepository<TModel> repository)
	{
		_repository = repository;
	}

	public virtual void Create(TModel model)
	{
		_repository.Create(model);
	}

	public virtual void Delete(int id)
	{
		_repository.Delete(id);
	}

	public virtual TModel? Get(int id)
	{
		return _repository.Get(id);
	}

	public virtual IReadOnlyCollection<TModel> GetAll()
	{
		return _repository.GetAll();
	}

	public virtual void Update(TModel model)
	{
		_repository.Update(model);
	}
}
