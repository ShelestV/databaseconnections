﻿using ToDoApp.Domain.Models;

namespace ToDoApp.Application.Shared;

public interface ICrudRepository<TModel> where TModel : Model
{
	void Create(TModel model);
	void Delete(int id);
	void Update(TModel model);
	TModel? Get(int id);
	IReadOnlyCollection<TModel> GetAll();
}
